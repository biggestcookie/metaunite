# MetaUnite
An augmented reality lesson on Unity game development, developed for the Meta 2 Headset.

Created for my undergraduate research from January to April 2018, this app teaches the user the basics of Unity game development in augmented reality using interactive models that can be touched and modified, and teaches lessons on Unity basics. These include GameObjects and their properties, attaching scripts, world transform and manipulating it with vectors, and Unity physics using rigidbodies and colliders. 

The app was developed in Unity Engine 2017.3, and was written using C# and the Meta 2 SDK 2.6.0.

You can watch the demo video [here](https://youtu.be/675Gq-szbrw).

## Project Information
Porting projects from previous SDK versions of the application seems to cause the application to break. While I usually could simply revert the commit where the SDK was changed, it seems that previous SDK versions are incompatible with the new firmware update on the headset that comes bundled with the new SDK update.
Meta SDK 2.7.0 comes with features that make it worth using; much improved hand tracking and environment tracking, an upgraded refresh rate of 72hz, and several other speedups and performance fixes. It is possible that the SDK update does not actually break my code; however, as I am unable to personally test on another machine at the time of writing, this repository exists to allow others to continue or study this work.
The **master** branch contains a previous commit when the application was working, on the previous 2.6.x SDK, while the **development** branch contains an attempted port of this commit to 2.7.0. Both branches were created for Unity 2017.3.x.

The transcript for the application can be found [here](https://docs.google.com/document/d/1LXl25eQe5kSdc9fOFHEzecI3ECoL55-LW0M9ThcSnI0/edit "here"), while additional information can be found on the project slides [here.](https://docs.google.com/presentation/d/1tjFpuN-b6aJKYbHRmUBn_0irOyNVwiU2FzTazsfEv9g/edit?usp=sharing "here")

## Features

All features listed are what the application includes, however may or may not actually work as intended. 

- The application features scenes with basic setups using the Meta SDK assets, such as the 3D Meta Camera, Meta Hands, and Meta Interface elements. The user can toggle between the use of Hands and keyboard with the keyboard shortcut F8. A sound will be heard when doing so.
- The calibration of the headset occurs whenever a scene is reloaded or a new scene is loaded. 
- Two chapters of the planned lessons, world position and physics with rigidbodies have scenes with demonstrations in them. 
- A work-in-progress code editor also exists in a separate scene.
